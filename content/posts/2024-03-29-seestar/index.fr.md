---
title: Nouveau Script et tutorial dédiés au Seestar de ZWO
author: Cyril Richard
show_author: true
date: 2024-03-29T00:00:00+00:00
featured_image: seestar.jpg
categories:
  - Nouvelles

---

Nous sommes heureux de vous annoncer la mise à disposition d'un script et d'un tutorial dédiés au nouveau télescope All-in-One Zwo, Seestar.

Le tutorial explique pas à pas comment utiliser le script, puis ensuite, comment traiter l'image empilée. Le script proposé a été étudié pour prétraiter automatiquement les images brutes enregistrées par le Seestar et donner le meilleur de l'image, de façon automatique.

Nous espérons qu'il saura répondre à vos attentes. Pour la marche à suivre, c'est [ici](../../../tutorials/seestar). Le lien pour télécharger le script se trouve dans le tutoriel.
