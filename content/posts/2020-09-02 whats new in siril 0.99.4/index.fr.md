---
title: Nouveautés de la version 0.99.4
author: Argonothe
date: 2020-09-06T11:35:34+00:00
url: /fr/2020/09/06/nouveautes-de-la-version-0-99-4/
featured_image: /wp-content/uploads/2020/09/comparison.png
categories:
  - Nouvelles

---

Siril 0.99.4, disponible [ici][1], a introduit beaucoup de nouvelles fonctionnalités. Dans cet article, nous allons vous présenter celles qui sont mises en avant.

## **32bits natif**


Le changement le plus important et surtout le plus attendu est le passage au 32 bits flottant natif pour tous les calculs. Le format FITS par défaut est dorénavant de type TFLOAT. Les fichiers ont donc une taille deux fois grandes qu’avant et la mémoire utilisée est également deux fois plus importante. Cependant, là ou on aurait pu s’attendre à des temps de calculs deux fois plus long, un gros travail d’optimisation a été effectué dans tout le code. Pour comparaison, sur une machine avec un cœur I9, 32Go de RAM DDR4 (une bonne machine), un empilement de 47 images prend 5min 24s avec la version 0.9.12 alors que seulement 2min01s sont nécessaires pour empiler les images 32bits sous la 0.99.4.


## **Nouveaux algorithmes de dématriçage**.


Avec l’introduction du 32bits flottant dans Siril, la nécessité de revoir complètement nos algorithmes de dématriçage s’est très vite imposée. En effet,  ceux-ci travaillaient uniquement en 16bits.



9 nouveaux algorithmes de dématriçage ont donc été implémenté dans Siril. Bien sûr, un utilisateur aura rarement besoin de changer celui-ci et donc, le choix de l’algorithme par défaut s’est avéré crucial. Notre choix s’est porté sur l’algorithme RCD : algorithme très performant pour les objets circulaires, telles les étoiles. Il permet d’obtenir des images de qualité identique à d’autres logiciels qui utilisent la technique du Bayer Drizzle. Il remplace l’algorithme VNG qui était celui par défaut dans Siril jusqu’à la version 0.9.12.


{{< figure src="comparison.png" link="comparison.png" caption="Comparison" >}}


## Suppression des artefacts des capteurs X-TRANS


Ces capteurs d’appareil photo Fuji sont connus pour avoir des artefacts lorsque l’on prend des images Bias ou Darks. En effet, un carré apparaît dans l’image et ce dernier se retrouve sur les images pré-traitées. Un algorithme a donc été introduit afin de supprimer cet artefact. Soit automatiquement pendant le prétraitement, soit en ligne de commande avant de pré-traiter.


 

{{< figure src="Xtrans_1_2.jpg" link="Xtrans_1_2.jpg" caption="Image avant / après d&#8217;un master dark d&#8217;un Fuji X-T20" >}}


## Compression des FITS


Il est maintenant possible de travailler avec des FITS compressés. La compression peut être avec ou sans perte. Bien que la première option peut faire peur à l’astrophotographe exigent, il est très difficile (voir impossible ?) de constater une différence entre une image non compressée et une compressée avec des paramètres raisonnables. Le temps de prétraitement peut cependant être augmenté de façon plus ou moins grande en fonction des algorithmes choisis.


 

{{< figure src="pref.png" link="pref.png" caption="Prefences" >}}


## Extraction de couche Ha, Ha/OIII


Ces nouvelles fonctions permettent d’extraire directement des couches Halpha ou Halpha/OIII à partir des images CFA, non dématricées. Ceci est très pratique lorsqu’on utilise un filtre Ha, ou un tout nouveau Duo Narrowband avec une camera couleur (ou un <abbr title="Appareil Photo Numérique">APN</abbr>).



{{< figure src="Ha_1.png" link="Ha_1.png" caption="Image CFA prétraitée" >}}

{{< figure src="Ha_2.png" link="Ha_2.png" caption="Images Ha extraite à partir de ce fichier." >}}



## Fonction Linear Match


Avant de combiner des images prises avec différents filtres dans l’outil de composition RVB, il faut généralement faire en sorte que celles-ci soient cohérentes entre elles. Sans cela, l’image finale possède un fort déséquilibre qu’il est difficile de traiter. C’est le rôle de ce nouvel outil qui prend une image référence en paramètre et transforme l’image chargée pour la faire « correspondre » à la référence.


## Extraction de fond de ciel appliquée à une séquence


L’extraction de gradient est quelque chose qui doit pratiquement être fait à tous les coups. Que cela soit à cause de la pollution lumineuse, de la Lune ou de n’importe quelle autre source de lumière, l’image empilée comporte un gradient qu’il faut enlever. Ce gradient est la somme des gradients de chaque image unitaire. Or, la monture bouge, l’intensité et la position de l’éclairage urbain changent dans le temps, etc … De fait, le gradient final est une somme complexe de différents gradients et par conséquent il peut être assez difficile de l’enlever. Pour remédier à ce problème, Siril permet de supprimer le gradient automatiquement sur chaque image pré-traitée (à faire avant alignement). Une nouvelle séquence est ainsi créée et l’image empilée sera beaucoup plus simple à traiter, même si un retrait de gradient peut encore être nécessaire.


![mtf](mtf.png)


## Nouveau format de fichier : Séquence FITS en fichier unique


Le format FITS est un format de fichier très utilisé en astrophotographie. Développé par et pour la NASA, il a su prendre sa place au sein de la communauté d’astrophotographe amateur.



Ce que l’on sait moins cependant, c’est qu’il est possible d’enregistrer plusieurs images au sein d’un seul fichier FITS : un peu à la manière d’un film. Siril permet dorénavant de travailler sur des fichiers FITS contenant une séquence. Pour cela il suffit de sélectionner Séquence FITS lors de la conversion, ou lors du pré-traitement. Dans les scripts, il s’agit de l’option -fitseq.


## Nouvel outil de déconvolution


L’outil de déconvolution a été entièrement réécrit. En plus d’être maintenant un outil « temps réel », l’algorithme est bien plus performant et travaille avec des masques qui sont construit automatiquement et de façon transparente pour l’utilisateur. Les étoiles sont donc bien mieux protégées et les artefacts évités.


![deconv](deconv.png)


## Nouveaux scripts universels


Dans les versions de Siril antérieures à la v0.99.4, les scripts fournis avec Siril étaient destinés aux <abbr title="Appareil Photo Numérique">APN</abbr> seuls. Maintenant, les scripts travaillent peu importe le format de l’image d’entrée (FITS ou RAW). On retrouve également des scripts d’extraction du canal Ha : à la fin on se retrouve avec une image monochrome contenant le signal Ha, puis également un script d’extraction Ha/OIII. Cette fois on se retrouve avec deux images monochromes Ha et OII.


## Standardisation du format fit

Enfin, j&#8217;aimerais terminer par l&#8217;effort que nous avons fait pour rendre le fichier FITS plus standard dans la communauté des astrophotographes amateurs en introduisant un nouveau mot-clé FITS. Ce mot-clé a été rapidement adopté par de nombreux logiciels (même PixInsight qui a abandonné le support du FITS), ce qui montre qu&#8217;il était nécessaire. Pour en savoir plus sur cette nouvelle fonctionnalité, vous pouvez lire cette page :<https://free-astro.org/index.php?title=Siril:FITS_orientation>

Et bien entendu, beaucoup beaucoup &#8230; d&#8217;autre nouvelles fonctionnalités.

 [1]: https://www.siril.org/2020/08/13/siril-1-0-0beta/
