---
title: An article from “Libre Graphics World” talks about Siril
author: Argonothe
date: 2020-02-04T18:00:46+00:00
featured_image: /wp-content/uploads/2020/02/Capture-d’écran-du-2020-02-04-18-58-13-1024x681.png
categories:
  - News

---

Take time to read this new: **Week recap** from &#8220;Libre Graphics World&#8221; 🙂

Follow this link: http://libregraphicsworld.org/blog/entry/week-recap-3-february-2020
