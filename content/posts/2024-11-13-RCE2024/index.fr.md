---
title: Siril aux RCE 2024
author: Cyril Richard
show_author: true
date: 2024-11-13T00:00:00+00:00
featured_image: RCE_2024.jpg
categories:
  - News

---

Nous avons eu le plaisir de participer aux [**Rencontres du Ciel et de l’Espace 2024**](https://www.afastronomie.fr/rencontres-ciel-espace) à Paris, à la Villette, un événement incontournable organisé par l’Association Française d’Astronomie (AFA). Cet événement s’est déroulé les **9, 10 et 11 novembre**, et notre présentation a été un franc succès : la salle était comble, et malheureusement, tout le monde n’a pas pu y accéder.

{{<figure src="pano.jpg">}}

Lors de cette présentation, nous avons eu l’opportunité de dévoiler les **nouvelles fonctionnalités de Siril 1.4.0**, qui n’est pas encore sortie. Ce fut un moment incroyable, partagé avec des passionnés d’astronomie et d’imagerie.

Pour ceux qui n’ont pas pu assister, nous avons filmé la présentation, et elle est désormais disponible sur YouTube.

Merci à tous ceux qui sont venus et qui continuent à soutenir le développement de Siril !

{{< youtube 23rZ92g4Fzs >}}
