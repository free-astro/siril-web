---
title: Nous avons besoin de vous !
author: Cyril Richard
show_author: true
date: 2024-12-23T00:00:00+00:00
featured_image: need_u.png
categories:
  - News
---

Nous sommes ravis d'annoncer que Siril 1.4 est en route ! Bien que nous n'ayons pas encore de date de sortie précise, nous voulions vous donner un aperçu des principales nouveautés et des opportunités pour la communauté de s'impliquer.

## SPCC

L'un des principaux points forts est le nouvel outil de **calibration colorimétrique par spectrophotométrie (SPCC)**, qui permettra d'effectuer un étalonnage des couleurs très précis. Cependant, pour exploiter tout son potentiel, nous avons besoin d'une base de données complète contenant des informations sur les capteurs (pour les caméras monochromes et couleur) ainsi que des données sur les filtres. C'est là que **vous** entrez en jeu ! Nous construisons une base de données ouverte et accessible, et nous invitons la communauté à y contribuer.

Vous pouvez accéder à la base de données ici : [Base de données SPCC de Siril](https://gitlab.com/free-astro/siril-spcc-database). Sur la [page de documentation de Siril 1.4](https://siril.readthedocs.io/en/latest/processing/color-calibration/spcc-database.html#converting-the-data), nous vous expliquons les étapes pour contribuer à cette ressource précieuse.

## Traduction

Mais ce n'est pas tout ! Nous recherchons également des volontaires pour nous aider à **traduire le logiciel et sa documentation**. Si vous souhaitez contribuer, que vous soyez bilingues ou plus, nous serions ravis de vous comptez parmi nous. Cela permettra de rendre Siril accessible à un public plus large dans le monde entier. Pour commencer, créez simplement un compte ici : [Traduction de Siril sur Weblate](https://weblate.siril.org/projects/siril/).

N'hésitez pas à nous contacter si vous souhaitez contribuer de quelque manière que ce soit ! Ensemble, nous pouvons améliorer Siril pour toute la communauté.

Nous vous souhaitons d'excellentes fêtes de fin d'année, et nous avons hâte de vous retrouver en 2025 pour la suite de cette belle aventure avec Siril !

{{<figure src="celebration.jpg">}}
