---
title: We need you!
author: Cyril Richard
show_author: true
date: 2024-12-23T00:00:00+00:00
featured_image: need_u.png
categories:
  - News
---

We’re excited to announce that Siril 1.4 is on its way! While we don't have a set release date yet, we wanted to share a sneak peek of some standout features and opportunities for the community to get involved.

## SPCC

One of the main highlights is the new **SpectroPhotometry Color Calibration (SPCC)** tool, which will allow users to perform highly precise color calibration. However, to maximize its potential, we need a comprehensive database containing sensor information (for both monochrome and color cameras) and filter data. That’s where **you** come in! We’re building an open and accessible database, and we invite the community to contribute.

You can access the database here: [Siril SPCC Database](https://gitlab.com/free-astro/siril-spcc-database). In our [Siril 1.4 documentation page](https://siril.readthedocs.io/en/latest/processing/color-calibration/spcc-database.html#converting-the-data), we’ll walk you through the steps on how to contribute to this valuable resource.

## Translation

But that's not all! We’re also seeking volunteers to help **translate the software and its documentation**. If you're interested in contributing, whether you're fluent in one language or several, we’d love to have you on board. This will ensure that Siril is accessible to a wider audience around the world. To get started, simply create an account here: [Weblate Siril Translation](https://weblate.siril.org/projects/siril/).

Feel free to reach out if you're interested in contributing in any way! Together, we can make Siril even better for the entire community.

We wish you a wonderful holiday season, and we look forward to continuing this exciting journey with Siril in 2025!

{{<figure src="celebration.jpg">}}
