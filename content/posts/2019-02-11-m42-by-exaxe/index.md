---
title: M42 by Exaxe
author: Argonothe
date: 2019-02-11T16:34:25+00:00
featured_image: m42-1024x716.jpg
categories:
  - Lucky imaging
---

Stephane Gonzales (Exaxe) shot M42 in deepksy lucky imaging. The result is so stunning that we want to share you the result.

Take a seat, make your comfortable and enjoy.

The English thread is on the Cloudy Night [forum][1].

To watch the full resolution follow the [link][2].

 [1]: https://www.cloudynights.com/topic/650427-m42-orion%E2%80%99s-trapezium-and-some-proplyds-with-short-exposures/?hl=%2Bsiril
 [2]: https://cdn0.webastro.net/uploads/monthly_2019_02/1637999208_m422019.jpg.05d3e5ee970829953aafe189e0749903.jpg
