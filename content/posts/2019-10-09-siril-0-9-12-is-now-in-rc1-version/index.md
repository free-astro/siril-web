---
title: Siril 0.9.12 is now in RC1 version
author: Argonothe
date: 2019-10-09T05:10:49+00:00
featured_image: Capture-du-2019-10-09-07-09-03.png
categories:
  - News
---

The development of the new version of Siril continues, the version is now RC1&hellip;.

This development version is available under Linux.
