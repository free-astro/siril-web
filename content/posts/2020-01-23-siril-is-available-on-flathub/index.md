---
title: Siril is available on flathub
author: Argonothe
date: 2020-01-23T08:15:29+00:00
featured_image: /wp-content/uploads/2020/01/Capture-d’écran-du-2020-01-23-09-08-18-1024x517.png
categories:
  - News
  - Tips
---

<a href="https://flathub.org/apps/details/org.free_astro.siril" title="Siril on flathub">
<img loading="lazy" src="Capture-d’écran-du-2020-01-23-09-13-04.png" alt="Screenshot of Siril in flathub" />
</a>


## Command line instructions

### Install:

#### Make sure to follow the [setup guide][1] before installing

`flatpak install flathub org.free_astro.siril` 

##### Run:

`flatpak run org.free_astro.siril`

 [1]: https://flatpak.org/setup/
