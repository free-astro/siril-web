---
title: Les Douze Jours de Siril
author: Adrian Knagg-Baugh 
show_author: true
date: 2024-12-24T00:00:00+00:00
featured_image: 12days.jpg
categories:
  - Nouvelles

---

L'équipe Siril vous souhaite un très joyeux Noël. Dans l'esprit de cette période, nous sommes ravis de présenter ces douze cadeaux de Noël qui mettent en valeur les énormes avancées à venir dans Siril 1.4. Nous n'avons pas encore de date de sortie car il reste (très) peu de détails à finaliser, mais nous avons hâte d'annoncer une version bêta début 2025. N'oubliez pas que [nous avons également besoin de vous !](../nous-avons-besoin-de-vous)

## Jour 1 : Alignement Astrométrique

Siril 1.4 introduit l'alignement astrométrique avec correction de distorsion polynomiale jusqu'au cinquième ordre. Cela améliore grandement la précision de l'alignement et prend également en charge la correction de distorsion, ce qui signifie que les images avec des points centraux significativement différents peuvent être alignées beaucoup plus précisément. Cela génère également de bien meilleurs résultats de PCC et SPCC car les étoiles du catalogue correspondent maintenant beaucoup plus précisément aux positions des étoiles détectées dans l'image.

## Jour 2 : Algorithme Drizzle du Télescope Spatial Hubble

Dans les versions précédentes de Siril, l'algorithme "Drizzle simplifié" n'était guère plus qu'une mise à l'échelle glorifiée appliquée à chaque image avant l'empilement. Bien que cela puisse offrir un petit avantage, l'effet n'était pas aussi important que le véritable algorithme de Reconstruction Linéaire à Pixels Variables rendu célèbre par l'équipe du télescope spatial Hubble. Nous avons maintenant implémenté le véritable algorithme Drizzle et les résultats sont nettement meilleurs sur les images sous-échantillonnées. De plus, il fonctionne sur les images Bayer : non seulement cela fournit de meilleurs résultats que l'utilisation d'algorithmes de débayerisation classiques, mais cela économise également une quantité importante d'espace disque intermédiaire, car les différentes opérations de séquence généralement effectuées avant l'alignement (calibration, extraction de fond linéaire) ont été revues et mises à jour pour s'assurer qu'elles fonctionnent directement avec des séquences CFA non débayerisées.

## Jour 3 : Empilement de Mosaïques

L'empilement de mosaïques est certainement la fonctionnalité la plus demandée pour Siril et nous sommes extrêmement fiers de la présenter dans la version 1.4, s'appuyant sur le travail d'alignement astrométrique mentionné ci-dessus. Maintenant, les images couvrant une mosaïque complète peuvent être alignées ensemble et empilées, et un algorithme de fusion des bords est utilisé pour minimiser les artéfacts de fusion.

## Jour 4 : Gestion des Couleurs

Siril 1.4 introduira un flux de travail entièrement géré en couleur. Vous pouvez maintenant vous assurer que les couleurs de votre image apparaissent aussi fidèlement que possible sur n'importe quel périphérique de sortie, que ce soit votre moniteur, le moniteur de votre public sur Internet ou même une copie papier. Vous pouvez éditer dans des espaces colorimétriques à large gamme et mieux utiliser les moniteurs à large gamme, et vous pouvez convertir les images entre différents espaces colorimétriques. En interne, nous utilisons les moteurs de gestion des couleurs Little CMS hautement respectés et optimisés, et tirons parti de ses modules d'accélération sous licence GPL pour améliorer la vitesse des transformations. Siril dispose d'un support intégré pour les profils sRGB et Rec.2020, mais vous pouvez facilement charger d'autres profils depuis le disque si vous préférez d'autres espaces colorimétriques tels qu'Adobe RGB, P3, ProPhoto, etc.

Parallèlement à la gestion des couleurs utilisant les profils ICC, nous avons introduit un aperçu d'affichage ISO 12646. Cela montre l'image avec un mode d'aperçu linéaire et entourée d'une bordure gris moyen et d'un cadre blanc, fournissant des conditions de visualisation reproductibles et standardisées pour vous aider à évaluer votre création.

## Jour 5 : Étalonnage Couleur Spectrophotométrique

Depuis que Siril a introduit l'étalonnage des couleurs par photométrie, la sortie du catalogue Gaia DR3 a révolutionné l'accès aux données astrométriques et photométriques de haute qualité. Il est maintenant disponible comme catalogue en ligne pour l'astrométrie mais permet également une amélioration de l'étalonnage des couleurs par photométrie. En accédant aux données spectrales réelles d'un échantillon d'étoiles dans l'image, ainsi qu'aux données sur les performances de votre capteur et filtres spécifiques, nous pouvons générer une comparaison plus précise entre le flux rouge, vert et bleu attendu basé sur le catalogue et ce qui est réellement capturé dans vos images, et faire les corrections nécessaires beaucoup plus précisément que jamais.

Ce n'est pas la dernière étape dans l'utilisation des données du satellite Gaia : nous travaillons actuellement sur l'implémentation d'un extrait hors ligne de Gaia DR3 pour les besoins astrométriques et photométriques, bien que cela sera probablement introduit un peu plus tard que la plupart de ces offres. Tout cela nous préparera également à profiter des futures améliorations en précision et couverture avec les sorties de Gaia DR4 prévues en 2026 et le catalogue final Gaia DR5 attendu vers la fin de la décennie.

## Jour 6 : Nouveaux Étirements et Filtres

Siril 1.4 présente des contributions de deux nouveaux contributeurs notables. M. Wadowski a contribué un outil de courbes qui permet d'étirer et d'ajuster le contraste de votre image en utilisant des segments de courbe linéaires ou spline cubique.

Ian Cass (qui aide également à générer le futur extrait de catalogue Gaia hors ligne) a contribué un nouveau filtre "unpurple" pour aider à combattre les aberrations chromatiques autour des étoiles.

Nous accueillons toutes les contributions, non seulement de code mais aussi le travail acharné de notre réseau de traducteurs. Si vous avez une idée et souhaitez vous impliquer, n'hésitez pas à nous contacter !

## Jour 7 : Interface GraXpert

Une autre fonctionnalité très demandée a été l'ajout d'une interface pour supporter GraXpert. Celle-ci fera ses débuts dans Siril 1.4 et prend en charge toutes les opérations GraXpert (extraction de fond, débruitage et les nouvelles opérations de déconvolution, y compris tous les modes IA).

Veuillez noter que, comme avec Starnet, GraXpert est un logiciel tiers, il est donc important de s'assurer qu'il fonctionne par lui-même avant de s'attendre à ce qu'il fonctionne directement depuis Siril.

## Jour 8 : Traitement par Région d'Intérêt

Certaines des opérations graphiques fournies par Siril peuvent être lentes (oui déconvolution, je te regarde !) donc Siril 1.4 introduit un système d'aperçu ROI où vous pouvez explorer l'effet de différents paramètres sur une petite région de l'image avant de l'appliquer à l'image entière. Toutes les opérations d'image ne prennent pas en charge le traitement ROI, mais toutes celles qui en bénéficient le plus le font.

## Jour 9 : Améliorations de l'Interface Utilisateur

Siril 1.4 voit des améliorations de l'interface utilisateur. Un nouveau menu "Outils" a été introduit pour les opérations non graphiques qui étaient auparavant difficiles à trouver dans le menu hamburger. Le menu Scripts a également vu des améliorations - je parlerai plus des améliorations de scripting ci-dessous ! De plus, le comportement des éléments UI dans les dialogues a fait l'objet d'un travail pour améliorer la cohérence du comportement entre les différents dialogues.

## Jour 10 : Optimisations et Améliorations Sous le Capot

Depuis la série stable 1.2, beaucoup de travail a été fait sous le capot. Une grande partie de cela n'est pas facilement visible pour l'utilisateur final, mais par exemple la division du fichier d'interface utilisateur unique en un grand nombre de fichiers plus petits a rendu le code beaucoup plus maintenable et a grandement réduit les conflits de fusion entre différents contributeurs travaillant simultanément sur différentes choses.

D'autres améliorations sous le capot incluent des optimisations (environ 2x d'accélération dans le code de Transformation Hyperbolique Généralisée, et une utilisation de la mémoire grandement améliorée dans la déconvolution), des améliorations de la gestion des mots-clés FITS, des améliorations de PixelMath, une refonte du code réseau et des améliorations de l'interaction avec les outils externes dans des environnements tels que flatpak.

## Jour 11 : Corrections de Bugs

Beaucoup de bugs ont été corrigés dans la série 1.2, y compris le bug notoire et de longue date spécifique à MacOS concernant le calcul de l'espace libre (purgeable). Toutes ces corrections ont été appliquées à 1.4 également, et en plus quelques corrections de bugs figureront dans 1.4 qui n'étaient pas possibles à rétroporter vers 1.2 en raison de l'étendue des changements de code associés. Le cycle de développement 1.4 a également intensifié l'utilisation de Coverity Scan pour l'analyse statique, ce qui permettra de réduire significativement les bugs liés à la qualité du code par rapport aux versions précédentes.

## Jour 12 : Refonte du Scripting

Peut-être le changement le plus radical à venir dans 1.4 est une énorme refonte du scripting Siril. L'idée a commencé comme un simple désir de faciliter l'intégration d'une plus large gamme d'outils tiers, mais cela a rapidement escaladé ! En conséquence, Siril introduira le scripting Python. Vous pourrez écrire des scripts pour Siril en Python. Non seulement cela donne accès à toutes les commandes Siril existantes en utilisant une méthode `cmd()`, mais cela permet d'écrire des scripts qui prennent des entrées utilisateur via une interface graphique TKinter ou en passant des arguments depuis la nouvelle commande Siril `pyscript`, et cela permet aux auteurs de scripts d'utiliser une gamme de modules Python tels que NumPy, astropy et autres. Il y a encore des limitations sur les modules qui peuvent être utilisés, et en raison de l'ampleur et de la complexité de ce changement, la nouvelle capacité doit être considérée comme expérimentale, bien que nous ayons l'intention de la stabiliser à travers les versions bêta 1.4 et d'arriver à un gel initial de l'API à 1.4.0-rc1.

En parlant de l'API, l'interface est entièrement documentée en interne à l'aide de docstrings et le manuel en ligne de Siril contient une documentation complète de l'API générée automatiquement à partir des docstrings en utilisant autodoc, elle sera donc toujours à jour.

Pour couronner le tout, nous avons ajouté un dépôt de scripts en ligne et encourageons les utilisateurs à y soumettre des scripts ; nous avons également introduit un éditeur de scripts intégré fournissant toutes les fonctionnalités que vous attendez d'un mini-IDE, y compris la coloration syntaxique, la capacité de tester vos scripts directement depuis l'éditeur (y compris avec des arguments), des marqueurs d'espaces blancs visibles configurables et une mini-carte pour aider à la navigation dans les scripts longs.

Notez que bien que presque toutes les offres de Noël discutées ci-dessus soient disponibles pour prévisualisation dès maintenant sur [le site gitlab de Siril](https://gitlab.com/siril), le scripting Python a encore quelques détails à finaliser avant de pouvoir être fusionné - c'est la dernière fonctionnalité à terminer avant la préparation de 1.4.0-beta1 !

Nous vous souhaitons à tous de très joyeuses fêtes de fin d'année et des ciels clairs en 2025 !

l'équipe free-astro
