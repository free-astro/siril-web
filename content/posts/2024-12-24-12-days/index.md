---
title: The Twelve Days of Siril
author: Adrian Knagg-Baugh 
show_author: true
date: 2024-12-24T00:00:00+00:00
featured_image: 12days.jpg
categories:
  - News

---

The Siril team wish you all a very happy Christmas. In the spirit of the season we are excited to present these twelve Christmas offerings which showcase the huge advances coming soon in Siril 1.4. We don't have a release date just yet as there are (very) few remaining loose ends to tie up, but we look forward to announcing a beta release early in 2025. Don't forget that we also [need your help!](../we-need-you)

## Day 1: Astrometric Registration

Siril 1.4 introduces astrometric registration with up to fifth order polynomial distortion correction. This greatly improves the accuracy of registration and also supports distortion correction, which means that frames with significanty different centre points can be much more accurately registered. It also generates greatly improved results from PCC and SPCC as the catalogue stars now much more accurately correspond to the locations of the stars detected in the image.

## Day 2: Hubble Space Telescope Drizzle Algorithm

In previous versions of Siril the "simplified Drizzle" algorithm was little more than a glorified upscale applied to each frame prior to stacking. While this could offer a little benefit, the effect was not as great as the true Variable-Pixel Linear Reconstruction algorithm made famous by the Hubble Space Telescope team. We have now implemented the true Drizzle algorithm and the results are significantly better on undersampled images. In addition, it works on Bayer patterned images: not only does this provide better results than using classical debayering algorithms but it saves a significant amount of intermediate disk space as well, as the various sequence operations typically carried out before registration (calibration, linear background extraction) have been reviewed and updated to ensure they work directly with un-debayered CFA sequences.

## Day 3: Mosaic Stacking

Mosaic stacking must be the single most-requested feature for Siril and we are extremely proud to debut it in 1.4, building on the astrometric registration work mentioned above. Now frames covering a full mosaic can be registered together and stacked, and an edge feathering algorithm is used to minimize blending artefacts.

## Day 4: Colour Management

Siril 1.4 will introduce a fully colour managed workflow. You can now ensure the colours of your image look as close as possible to the same on any output device, be it your monitor, the monitor of your audience on the internet or even hard copy. You can edit in wide gamut colour spaces and make better use of wide gamut monitors, and you can convert images between different colour spaces. Internally we use the highly respected and optimized Little CMS colour management engines, and take advantage of its GPL-licensed acceleration modules to improve the speed of transforms. Siril has built-in support for sRGB and Rec.2020 profiles but you can easily load other profiles from disk if you prefer other colour spaces such as Adobe RGB, P3, ProPhoto etc.

Alongside colour management using ICC profiles we have introduced an ISO 12646 display preview. This shows the image with a linear preview mode and surrounded by a mid grey border and white frame, providing reproducible and standardized viewing conditions to help you appraise your creation.

## Day 5: Spectrophotometric Colour Calibration

Since Siril introduced photometric colour calibration the release of the Gaia DR3 catalogue has revolutionized access to high quality astrometric and photometric data. It is now available as an online catalogue for astrometry but also enables an enhancement of photometric colour calibration. By accessing the actual spectral data for a sample of stars in the image, together with data on the performance of your specific sensor and filters, we can generate a more accurate comparison between the expected red, green and blue flux based on the catalogue and what is actually captured in your images, and make the necessary corrections much more accurately than ever before.

This isn't the last step in use of data from the Gaia satellite either: we are currently working on implementing an offline extract of Gaia DR3 for astrometric and photometric purposes, although this will probably be introduced a little later than most of these offerings. Al of this will also set us up to take advantage of further improvements in accuracy and coverage with the releases of Gaa DR4 expected in 2026 and the final Gaia DR5 catalogue expected around the end of the decade.

## Day 6: New Stretches and Filters

Siril 1.4 features contributions from two notable new contributors. M. Wadowski has contributed a curves tool that allows stretching and contrast tweaking your image using either linear or cubic spline curve segments.

Ian Cass (who is also helping generate the forthcoming offline Gaia catalogue extract) has contributed a new "unpurple" filter to help in combating chromatic aberrations around stars.

We welcome all contributions, not only of code but also the hard work of our network of translators. If you have an idea andwant to get involved please get in touch!

## Day 7: GraXpert Interface

Another highly requested feature has been addition of an interface to support GraXpert. This will premiere in Siril 1.4 and supports all GraXpert operations (background extraction, denoising and the new deconvolution operations, including all the AI modes).

Please note that, as with Starnet, GraXpert is third party software so it is important to ensure it works by itself before expecting it to work directly from Siril.

## Day 8: Region of Interest Processing

Some of the graphics operations provided by Siril can be slow (yes deconvolution, I'm looking at you!) so Siril 1.4 introduces a ROI preview system where you can explore the effect of different parameters on a small region of the image before applying it to the entire image. Not all image operations support ROI processing, but all the ones that benefit most do.

## Day 9: User Interface Improvements

Siril 1.4 sees improvements to the user interface. A new "Tools" menu has been introduced for non-graphical operations that were previously hard to find in the hamburger menu. The Scripts menu has also seen improvements - I'll talk more about improvements to scripting below! In addition the behaviour of UI elements within dialogs has seen work to improve consistency of behaviour between different dialogs.

## Day 10: Optimisations and under-the-hood Improvements

Since the 1.2 stable series a lot of work has gone on under the hood. Much of this is not easily visilbe to the end user, but for example splitting up the single user interface file into a large number of smaller files has made the code much more maintainable and has greatly reduced merge conflicts between different contributors working on different things simultaneously.

Other under-the-hood improvements include optimisations (about a 2x speedup in Generalized Hyperbolic Transform code, and greatly improved memory use in deconvolution), improvements to FITS keyword management, enhancements to PixelMath, refactoring of networking code and improvements to interaction with external tools in environments such as flatpak.

## Day 11: Bug fixes

A lot of bugs have been fixed in the 1.2 series, including the notorious and long-lived MacOS-specific bug with calculation of free (purgeable) space. All of these fixes have been applied to 1.4 as well, and in addition a few bug fixes will feature in 1.4 that were not possible to backport to 1.2 owing to the extent of related code changes. The 1.4 development cycle has also seen greatly increased use of Coverity Scan for static analysis, which will result in far fewer bugs relating to basic code quality than ever before.

## Day 12: Scripting Overhaul

Perhaps the most far-reaching change coming in 1.4 is a huge overhaul of Siril scripting. The idea began as a simple desire to make it easier to integrate a wider range of third party tools, but it escalated quickly! As a result Siril will introduce python scripting. You will be able to write scripts for Siril in python. Not only does this give access to all the existing Siril commands using a `cmd()` method, but it allows writing scripts that take user input via a TKinter GUI or by passing arguments from the new Siril `pyscript` command, and it allows script writers to utilise a range of python modules such as NumPy, astropy and others. There are still limitations to what modules can be used, and owing to the scale and complexity of this change the new capability shoudl be regarded as experimental, though we intend to stabilize it through the 1.4 beta releases and come to an initial API freeze at 1.4.0-rc1.

Speaking of the API, the interface is fully documented internally using docstrings and the online Siril manual contains full API documentation automatically generated from the docstrings using autodoc, so it will always be up-to-date.

To top it off, we have added an online scripts repository and encourage users to submit scripts to it; we have also introduced a built-in script editor providing all the features you would expect of a mini-IDE including syntax highlighting, the ability to test your scripts directly from the editor (including with arguments), configurable visible whitespace markers and a mini-map to aid navigation through long scripts.

Note that while almost all of the Christmas offerings discussed above are available for previewing right now at [the Siril gitlab site](https://gitlab.com/siril) the Python scripting has a couple of loose ends to be finished off before it can be merged - it is the final feature to be finished off before preparation of 1.4.0-beta1!

Wishing you all a very happy holiday season and clear skies in 2025!

team free-astro
