---
title: "Siril Man Page"
author: Cyril Richard
---

## Name
       siril - image processing tool for astronomy and others

## Synopsis
       Siril GUI
        siril [options] [file]             Start Siril.

       Siril CLI
        siril-cli  [-i  conf_file]  [-f]  [-v]  [-c]  [-h]  [-p]  [-d  working_directory]  
       [-s  script_file] [-r input_pipe_path] [-w output_pipe_path] [im‐
       age_file_to_open]

## Description
Siril is an image processing tool specially tailored for  noise  reduction  and  improving  the  signal/noise ratio of an image from multiple captures, as required in astronomy. Siril can align, stack and  enhance pictures  from  various  file formats, even image sequences (movies and SER files). It makes use of OpenMP, supports all the cameras  supported by  libraw, and carries out its calculations in a high precision 32-bit floating point engine.

## Options
       -i     Starts Siril with configuration file  which  path  is  given  in
              conf_file

       -f     (or  --format) Prints all supported image input formats, depend‐
              ing on the libraries detected at compile-time

       -v     (or --version) Prints program name and version and exits
       
       -c     (or --copyright) Prints copyright and exits

       -h     (or --help) Short usage help

       -p     Starts Siril without the graphical user interface  and  use  the
              named  pipes to accept commands and print logs and status infor‐
              mation. On  POSIX  systems,  the  named  pipes  are  created  in
              /tmp/siril_commands.in and /tmp/siril_commands.out
              
       -r     (or --inpipe) Provide an alternative path for the input pipe that 
              receives the commands, if -p is passed. The pipe can be create by 
              an  external program with the mkfifo(1) command

       -w     (or --outpipe) Provide an alternative path for the output pipe 
              that prints logs and status updates, if -p is passed. The pipe can 
              be create by an external program with the mkfifo(1) command

       -d     (or --directory) Setting argument in cwd

       -s     (or  --script) Starts Siril without the graphical user interface
              and run a script instead. Scripts are text files that contain  a
              list  of  commands  to be executed sequentially. In these files,
              lines starting with a # are considered as comments.

       image_file_to_open
              Open an image or sequence file right after start-up

## Files
       ~/.config/siril/siril.cfg
              User preferences. Overridden by the -i option.
       ~/.siril/siril.css
              The stylesheet used to change colours of the graphical user  in‐
              terface. This is useful for personalisation or if a GTK theme is
              incompatible with some coloured elements of Siril.

## Links
1. Website with news, tutorials, FAQ and more: https://siril.org
1. Forum: https://discuss.pixls.us/siril
1. Documentation: https://siril.readthedocs.io/en
1. Bug tracker: https://gitlab.com/free-astro/siril/issues

## Authors
* [Vincent Hourdin](debian-siril@free-astro.org)
* [Cyril Richard](cyril@free-astro.org)
* [Cécile Melis](cissou8@gmail.com)
* [Adrian Knagg-Baugh](aje.baugh@gmail.com)

## Copyright
* Copyright © 2021 Free Software Foundation, Inc.  License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.  There is NO WARRANTY, to the extent permitted by law.

