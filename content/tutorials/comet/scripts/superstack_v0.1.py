##################################################
# Jan 2022
# SuperStack V0.1
# C. Melis (C)
# This script stacks multiple images of a sequence
# to create superstacks
##################################################
import os,sys
from pathlib import Path
from pysiril.siril import Siril
from pysiril.addons import Addons
from pysiril.wrapper import Wrapper

# User settings
# nbframes: number of frames in each superstack
# step: step between each superstack
# seqname: name of the registered sequence
# stackfolder: output subfolder
# processfolder: name of the subfolder which contains the sequence
# ext: chosen fits extension

def Run(nbframes = 3, step = 1, seqname = 'comet_', stackfolder = 'superstack',  processfolder = 'process', ext = 'fit'):

    #recasting types if using script mode
    nbframes=int(nbframes)
    step=int(step)
    print('Number of frames per superstack: {0:d}'.format(nbframes))
    print('Step between each stack: {0:d}'.format(step))
    print('Processing sequence: {0:s}'.format(os.path.join(os.getcwd(),processfolder,seqname+'.seq')))
    print('Superstack saved to folder: {0:s}'.format(stackfolder))
    print('FITS extension: {0:s}\n'.format(ext))

    #preparing pysiril
    workdir = os.getcwd()
    print('Starting PySiril')
    app = Siril()
    AO = Addons(app)
    cmd = Wrapper(app)

    #stacking method
    method='mean 3 3'
    # temporary folder name (will be deleted at the end)
    tempfolder='tmp'
    # prepare tmp and out folders
    # do not force creation to give a chance to correct
    # before overwriting
    try:
        Path(tempfolder).mkdir(exist_ok=False)
    except:
        print('\n{:s} already exists - aborting'.format(tempfolder))
        sys.exit()
    try:
        Path(stackfolder).mkdir(exist_ok=False)
    except:
        os.rmdir(tempfolder)
        print('\n{:s} already exists - aborting'.format(stackfolder))
        sys.exit()

    #parse the .seq file to find total number of frames
    seqfile = AO.GetSeqFile(os.path.join(workdir,processfolder,seqname+'.seq'))
    total=seqfile['nb_images']
    print('\n\nNumber of frames in the sequence: {0:d}\n\n'.format(total))

    print('Starting Siril')
    try:
        app.tr.Configure(False,False,True,True)
        app.MuteSiril(True)
        app.Open()
        NS=1
        NE=NS+nbframes-1
        c=0
        app.Execute('setext {:s}'.format(ext))
        app.Execute('cd {:s}'.format(workdir))
        while (NE<=total):
            print('Superstack#{0:d}: {1:d}-{2:d}'.format(c+1,NS,NE))
            for i in range(nbframes):
                src='{0:s}{1:05d}.{2:s}'.format(seqname,seqfile['images'][NS+i-1]['filenum'],ext)
                os.symlink(os.path.join(workdir,processfolder,src),os.path.join(workdir,tempfolder,src))
            cmd.cd(tempfolder)
            cmd.Execute('stack '+seqname+' '+method+' -nonorm')
            cmd.cd('..')
            stack = '{0:s}stacked.{1:s}'.format(seqname,ext)
            supstack = '{0:s}_{1:s}{2:05d}.{3:s}'.format(stackfolder,seqname,c+1,ext)
            os.link(os.path.join(workdir,tempfolder,stack),os.path.join(workdir,stackfolder,supstack))

            for f in os.listdir(os.path.join(workdir,tempfolder)):
                os.remove(os.path.join(workdir,tempfolder,f))
            NS=NS+step
            NE=NS+nbframes-1
            c+=1
        print('Number of superstacks: {0:d}'.format(c))
    except Exception as e :
        print("\n**** ERROR *** " +  str(e) + "\n" )
    os.rmdir(tempfolder)
    app.Close()
    del app

if __name__ == "__main__":
    args=[]
    kwargs={}
    for a in sys.argv[1:]:
        if '=' in a:
            f,v=a.split('=',1)
            kwargs[f]=v
        else:
            args.append(a)
    Run(*tuple(args),**kwargs)
