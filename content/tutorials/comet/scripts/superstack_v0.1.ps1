##################################################
# Jan 2022
# SuperStack V0.1
# C. Melis (C)
# This script stacks multiple images of a sequence
# to create superstacks
# It assumes frame numbers are contiguous
# in the input sequence and image numbering
# starts at 1
##################################################

# User settings
# nbframes:number of frames in each super stack
# step: number of frames between each super stack
# seqname: name of the registered sequence
# stackfolder: output subfolder
# processfolder: name of the subfolder which contains the sequence
# ext: chosen fits extension

# User command line inputs if any
param ($nbframes=3, $step=1, $seqname="comet_", $stackfolder="superstack", $processfolder="process", $ext="fit" )
"Number of frames per superstack: {0:d}" -f $nbframes
"Step between each stack: {0:d}" -f $step
"Processing sequence: \{0:s}\{1:s}.seq" -f $processfolder,$seqname
"Superstack saved to folder: {0:s}" -f $stackfolder
"FITS extension: {0:s}" -f $ext

#stacking method
$method="mean 3 3"

# temporary folder name (will be deleted at the end)
$tempfolder="tmp"

# path to siril-cli
$sirilcliexe="C:\Program Files\SiriL\bin\siril-cli.exe"

# prepare tmp and out folders
# do not force creation to give a chance to correct
# before overwriting
try{
  $null = New-Item -Path $tempfolder -ItemType directory -ErrorAction Stop
}
catch [System.IO.IOException]
{
    Write-Output $tempfolder" : Directory Exists Already"
    exit 1
}
try{
  $null = New-Item -Path $stackfolder -ItemType directory -ErrorAction Stop
}
catch [System.IO.IOException]
{
    Write-Output $stackfolder" : Directory Exists Already"
    exit 1
}

#parse the .seq file to find total number of frames
$line=Get-Content $processfolder\$seqname.seq | Select-String -Pattern '^[^#]' | select-object -First 1
$total=($line -split ' ')[3]
""
"Number of frames in the sequence: {0:d}" -f $total

# get siril version
$version=($(& $sirilcliexe --version) -split ' ')[-1]


# looping until the last frame of a superstack
# is larger than total number of frames
$NS=1
$NE=$NS+$nbframes-1
$c=0
while ($NE -le $total)
{
  Write-Host ("Superstack#{0:d}: {1:d}-{2:d}" -f $($c+1),$NS,$NE)
  for ($i = 1 ; $i -le $nbframes ; $i++){
    $SRC= "{0:s}{1:00000}.{2:s}" -f $seqname,$($NS-1+$i),$ext
    $null=cmd /c mklink $PWD\$tempfolder\$SRC $PWD\$processfolder\$SRC
  }
  @"
requires $version
setext $ext
cd $tempfolder
stack $seqname.seq $method -nonorm
cd ..
"@ | & $sirilcliexe -s - >log 2>&1

  $supstack="superstack_{0:s}{1:00000}.{2:s}" -f $seqname,$($c+1),$ext
  Move-Item -Path $tempfolder/${seqname}stacked.$ext -Destination $stackfolder/$supstack
  Get-ChildItem -Path $tempfolder -Include *.* -File -Recurse | ForEach-Object { $_.Delete()} #emptying temp folder
  $NS=$NS+$step
  $NE=$NS+$nbframes-1
  $c=$c+1
}

"Number of superstacks: {0:d}" -f $c
Remove-Item -path $tempfolder
