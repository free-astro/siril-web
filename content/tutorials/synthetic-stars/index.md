---
title: Synthetic Stars
author: Adrian Knagg-Baugh
show_author: true
featured_image: PMwindow.png
rate: "2/5"
type: page
---

This tutorial aims to explain the star synthesis tools available in Siril.
This feature helps improve the profiles of stars in Siril.

{{< table_of_contents >}}

# Astrometry and Star Synthesis
Siril has powerful astrometry tools that can detect stars in images and model them with various parameters including peak luminance, full width at half maximum, surrounding background level. Siril models stars as Gaussian profiles.

You can initiate star detection and modelling in several ways.
- Using the console command `findstar`
- Using the `Dynamic PSF` dialog (found in the hamburger menu -> `Image Information`)
- Siril tools can do it automatically in the background.
When you use one of the first two methods, Siril will detect stars and draw circles around all the ones it found. Orange circles mean the star is not saturated; blue circles mean it is saturated.

{{<figure src="lotsofstars.png" link="lotsofstars.png" caption="Siril is good at finding a lot of stars - over 13,000 in this image of the Crescent nebula!">}}

Star synthesis is simply the process of using the models of the detected stars to generate mathematical luminance profiles for stars. These profiles can be used in several ways to improve the appearance of images.

# What can star synthesis do for me?
There are two main uses of star synthesis.
- Fix saturated stars
- Fix stars with bad aberrations

# Synthetic Stars UI
The synthetic star tool can be accessed through the GUI and using Siril commands. The GUI is shown below:

{{<figure src="dialog.png" link="dialog.png" caption="Synthetic Star tool GUI">}}

The corresponding commands for use at the console or in scripts are:

- `findstar` - this command calls the star finder routine with default parameters: it can be thought of as the command equivalent of the `Dynamic PSF` GUI. It has optional parameters `-layer=n` for selecting the color channel to search for stars and `-maxstars=n` for limiting the maximum number of stars found. There is also a parameter `-out=/path/to/log.txt`, which is not required for preparing for star synthesis.
- `setfindstar` - this command sets the settings that findstar uses to detect stars. It is for advanced use and is therefore not covered in detail in this tutorial. Further details may be found in the in-Siril help.
- `unclipstars` - this command desaturates all saturated stars in the image.
- `synthstar` - this command generates a synthetic star mask, synthesizing all the stars in the current stars list or, if the list is empty, from a stars list generated using the default findstar parameters.

# Saturated Stars
The perfect image captures all the dynamic range of its subject, from faint wisps of nebulosity to bright first magnitude stars. In practice, our imaging sensors have limited dynamic range. So in order to capture the faint detail, we often have saturated stars. The recorded brightness around the centres of such stars is just a constant maximum level. This means that when the image gets stretched the star bloats and loses pretty much all of its colour - it's just a big fat burned-out white dot. It's the visual equivalent of the horrible noise you get from your speakers when you drive an amplifier into hard clipping.

{{<figure src="satstar.png" link="satstar.png" caption="Saturated stars near the Pelican nebula">}}

Stars like Sadr even look obviously bloated when the image is still unstretched linear data.

{{<figure src="satstar-linear.png" link="satstar-linear.png" caption="Sadr, obviously very saturated">}}

To improve this we can use star synthesis. Although the centre of the star profile is saturated, there is enough unsaturated detail around the edge to allow the point spread function (PSF) of the star to be modelled. The clipped pixels in the saturated star are replaced with the intensity from the synthesized PSF generated from the model Siril makes of the star, and the values of all pixels in the image rescaled so that the brightest point in the most saturated star is no brighter than the brightest value the image type (16 bit or 32 bit) can hold. This gives a nicely profiled star that can be stretched with less bloat.

{{<figure src="desat-linear.png" link="desat-linear.png" caption="The desaturated star has a softer, less bloated profile">}}

The effect is shown very clearly in a comparison of lines of pixel values through a star before and after desaturation.

{{<figure src="profiles.png" link="profiles.png" caption="The desaturated star has a softer, less bloated profile">}}

To do this is extremely easy: go to the `Image Processing` menu, `Star Processing`, `Desaturate Stars`. The desaturation process is automatic and requires no user input. Congratulations, you have now corrected the luminance profiles of all of your saturated stars!

# Star Replacement
Every now and then most of us take an image with appalling stars. Maybe we leave off the coma corrector; maybe we forgot to check collimation. Maybe we're just using a bad camera lens. It happens for various reasons.

{{<figure src="badstars.png" link="badstars.png" caption="Bad coma in an image of the Pacman nebula">}}

The best possible advice when this happens is: re-shoot your image. Yes, it can be maddening to throw away hours of imaging, but if you can easily re-shoot the image then you probably should. If the stars are terrible, the detail in the rest of the image won't be great either.

Sometimes though, that's not an option. Perhaps you took an image on holiday, of a nebula that isn't visible from your home latitude, or from a location that's difficult to reach, or on a special occasion. For whatever reason, you'd quite like to rescue it from having such awful stars.

You could try deconvolution but it's prone to image noise and it can be tricky to avoid ringing. There's only so much it can do for you. In this case, star synthesis (in conjunction with Starnet++) can save you.

The idea here is to use Starnet++ to separate your image into a starless image and a star mask. If you have a working Starnet++ installation it can be accessed directly from Siril. It generates a starless image and optionally a star mask as well. In this case, we want both.

Keep the starless image for later, but for now we open the star mask. In the `Image Processing` menu, find the `Star Processing` submenu item and open it.

The first thing we need to do is to detect all the stars in the image. You can do this using the `Dynamic PSF` tool, which is accessible via the toolbox button next to the `Full Resynthesis` menu entry, or alternatively from the hamburger menu under `Image Information`. You can adjust the settings to model the stars with Gaussian or Moffat star profiles: whichever profile you choose here is the profile that Resynthesis will use to generate your star mask. Use the stars button to find the stars in your image. All the stars in the image will have circles drawn around them. If you think Siril has missed the occasional star you can draw a selection around it and add it using the Ctrl-Space shortcut. Once you're content with the star list, you can close the `Dynamic PSF` tool - we don't need it any more.

Next, click the `Full Resynthesis` menu option. This will model a star luminance profile for every star in the list and add it into a synthetic star mask. If the original image was full colour the hue and saturation are copied across, so the colours of the synthetic star profiles will be exactly the same as in the original image.

If you have `AutoStretch` view mode on, the stars in the star mask will look very fat and bloated and lacking in detail. Don't worry, this is just a quirk of the viewer mode. They will look much better if you switch to the `Asinh` viewer mode, and this is representative of a typical stretch anyway.

Once you have your synthetic star profile you can mix it back into the starless image using Siril's `Star Recomposition` tool. This was originally written simply to blend the Starnet++ star masks with the starless images but it works just as well for blending the synthetic star masks. The two sides of the dialog allow you to stretch the starless image and the star mask separately using generalised hyperbolic stretches, so you can stretch the faint stuff good and hard to bring out the detail, and apply a much milder stretch to the stars in order to get the fainter ones to show but control bloat in the brighter ones.

{{<figure src="fixedstars.png" link="fixedstars.png" caption="Synthetic stars used to improve the image (noise reduction has also been applied to the starless image here)">}}
{{<figure src="Bubble_original.png" link="Bubble_original.png" caption="Another example of an image with imperfect stars">}}
{{<figure src="Bubble_synth.png" link="Bubble_synth.png" caption="After synthetic mask integration in Pixel Math">}}

# Tips
- It is very strongly recommended to do all star processing while the image is still linear, i.e. before it has been stretched. The process will still run if you input a stretched image, but results may be substantially worse than with a linear image.
- Siril's star detection and modelling expects stars to look approximately Gaussian. The more stretched they are, the less Gaussian they look, so detection performance may be worse and the models may not be as good.
- Starnet++ also works best with stretched input. (Or, to be more precise, it works best with a specific stretch applied: Siril's starnet integration can do this for you if you start with a linear image.)
- Synthetic Star Tools cannot synthesize diffraction spikes. (Yet...)
