---
title: Siril 0.99.4 (1.0.0 beta testing)
author: Cyril Richard
date: 2020-08-13T19:31:02+00:00
featured_image: /wp-content/uploads/2020/08/siril_logo.png
categories:
  - News
tags:
  - new release
version: "0.99.4"
linux_appimage: ""
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/Install_SiriL_0_99_4_64bits_EN-FR.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-0.99.4-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-0.99.4.tar.bz2"
---

We think it is time, time to release this new version for beta testing.

Since the last stable version, the 0.9.12, more that 1400 commits were pushed to our repository, more than 66 000 new lines of code were added. A lot of new developers helped us in optimization, new features and bug fixes. It is for sure our biggest improvement to the code of Siril.

This is why, unlike we used to do, we&#8217;ve decided to publish a beta version for the three most common platforms. This version, the 0.99.4 has been tested for some time by a few lucky users and should work in most cases. If you have issues with it or find bugs, we would be very grateful if you could contact us or report them to [our bug system tracker][1].

Release highlights:

  * Complete UI refactoring
  * 32-bit precision
  * New macOS and Windows packages
  * New universal scripts (DSLR/FITS)
  * Several bug fixes

**Contributors**:
Contributors to this release are: Cyril Richard, Vincent Hourdin, Ingo Weyrich (from RawTherapee) in a wonderful speed optimization work, Florian Benedetti, Alex Samorukov for the macOS bundle, Guillaume Roguez, Jehan, Sébastien Rombauts, Pascal Burlot, Fabrice Faure, Julian Hofer, Hubert Fuguière, Rafael Barberá, Schreiberste, Kristopher Setnes for X-TRANS work, Cecile Melis, Floessie, Emmanuel Brandt and François Meyer (!! the original author of Siril).

**Translators**:
Cyril Richard, Stefan Beck, Kostas Stavropoulos, Xiaolei Wu, Joaquin Manuel, Alexandre Prokoudine, Luca Benassi, Antoine Hlmn, Kenji Iohara

We also thank Colmic for the huge beta testing effort, Laurent Rogé for the documentation and all the [pixls.us][2] community!!

## Donate

Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development you&#8217;re welcome to donate a small amount of your choice.

<div style='margin-left: auto;margin-right: auto;width:220px'>
</div>

 [1]: https://gitlab.com/free-astro/siril/-/issues
 [2]: https://discuss.pixls.us/
