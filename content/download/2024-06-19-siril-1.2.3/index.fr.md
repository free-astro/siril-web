---
title: Siril 1.2.3
author: Cyril Richard
date: 2024-06-19T00:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /fr/download/1.2.3/
version: "1.2.3"
linux_appimage: "https://free-astro.org/download/Siril-1.2.3-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.2.3-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.2.3_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.2.3-3-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.2.3-3-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.2.3.tar.bz2"
---

### <span style="color:red">29 juillet, mise à jour importante pour les version macOS</span>

Le premier bug trouvé n'a pas été complètement corrigé, et de nombreux utilsateurs nous ont reporté des lenteurs. Nous invitons donc tous les utilisateurs macOS à télécharger le nouveau paquet, la révision 3, qui porte le numéro de build 11207.

### <span style="color:red">2 juillet, mise à jour importante pour les version macOS</span>

Un bug a été trouvé dans les version macOS empêchant l'utilisation de plusieurs threads pendant les calculs. Les deux paquets ont été mis à jour en version 2 (numéro de build 10977). Nous invitons tous nos utilisateurs macOS à télécharger les nouveaux paquets.

# La version

Bien que Siril 1.2.2 soit sorti très récemment, nous devons publier rapidement une nouvelle version, numérotée 1.2.3.

**Pourquoi ?** La raison est simple : plusieurs bugs très gênants ont été découverts. Certains affectent uniquement la version macOS, d'autres la version Windows, et un autre touche toutes les versions.

**Liste des bugs corrigés (depuis la 1.2.2)**

* Correction de la gestion des caractères non ascii dans les noms de fichiers sous Windows (cfitsio rollback) (#1324)
* Correction de l'erreur de compression (les fichiers n'étaient plus compressés) (#1328)
* Rétablissement de l'utilisation de la connexion internet dans la version macOS (problème de packaging)

# Sommaire

{{< table_of_contents >}}

# Téléchargements
Siril 1.2.3 est distribué comme d'habitude pour les 3 plateformes les plus courantes (Windows, MacOS, GNU / Linux). Voir la page [téléchargement](../../../fr/download).

Mais bien sûr, puisque Siril est un logiciel libre, il peut être construit sur n'importe quel OS à partir des sources, voir la page [installation](https://siril.readthedocs.io/fr/stable/installation/source.html).

# Une version de stabilisation

Encore une fois, cette version est une version de stabilisation de la série 1.2. Il n'y a donc aucune nouveauté, mais Siril devient plus stable grâce à tous les retours de la communauté. C'est pour cela qu'il est très important de nous remonter les bugs si vous en trouvez, c'est généralement le seul moyen de les voir corrigés dans la version suivante.

Nous vous remercions pour votre soutien continu et vos précieux retours. Téléchargez Siril 1.2.3 dès maintenant et profitez d'une version plus stable et fiable !

# Quoi de neuf avec Siril 1.4?

La future version 1.4 avance à pas de géant. Nous sommes encore dans le processus d'introduire de nouvelles fonctionnalités, mais nous devrions bientôt entrer dans une phase de stabilisation. Cette version représente une avancée majeure, presque une rupture avec les versions précédentes : un soin particulier a été apporté à l'ergonomie. De plus, le nombre de nouvelles fonctionnalités est très important et nous avons hâte de relâcher une version bêta pour que vous puissiez les découvrir. Restez à l'écoute, on ne sait jamais !


# Contribuer à Siril
Bien sûr, Siril est un logiciel informatique développé par des humains faillibles, et des bugs peuvent encore exister. Si vous pensez en découvrir, merci de nous [contacter](../../faq/#comment-puis-je-contacter-léquipe-de-siril-) ou d'[écrire](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) un rapport de bug si ce dernier n'est pas déjà présent dans la [liste des bugs connus](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened). Une [page de la documentation](https://siril.readthedocs.io/fr/stable/Issues.html) a été écrite dans le but de vous aider a reporter un problème.


# Dons
Développer des logiciels est amusant, mais cela nous prend aussi presque tout notre temps libre. Si vous aimez Siril et que vous souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un [don](../../donate) d’une somme de votre choix.
