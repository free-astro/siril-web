---
title: Siril 1.2.6
author: Cyril Richard
date: 2025-01-22T00:00:00+00:00
categories:
  - News
tags:
  - nouvelle version
aliases:
  - /download/1.2.6/
version: "1.2.6"
linux_appimage: "https://free-astro.org/download/Siril-1.2.6-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.2.6-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.2.6_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.2.6-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.2.6-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.2.6.tar.bz2"
---

### Notes de version de Siril 1.2.6
Nous sommes heureux d'annoncer la sortie de Siril 1.2.6. Bien que Siril 1.2.5 devait être la dernière mise à jour de la série 1.2, un bug critique dans la version Windows nous a obligés à publier un nouveau correctif. Ce bug a été introduit après une mise à jour récente de `cfitsio` qui causait des problèmes avec les noms de fichiers utilisant des caractères larges qui n'étaient pas encodés en UTF-8.

Cette mise à jour corrige principalement ce problème, garantissant que Siril fonctionne correctement avec les noms de fichiers non UTF-8 sous Windows. Nous vous remercions pour votre patience et vos retours qui nous ont aidés à résoudre cette question.

**Liste des bugs corrigés (depuis la version 1.2.5) :**
* Correction du problème avec les noms de fichiers utilisant des caractères larges sous Windows après la mise à jour de `cfitsio`.
* Correction d'erreurs de signe dans le catalogue Messier
* Correction d'un bug dans la fonctionalité Enregistrer sous
* Changement du répertoire de configuration sur macOS en org.siril.Siril

# Téléchargements
Siril 1.2.6 est disponible au téléchargement sur les plateformes les plus courantes (Windows, macOS, GNU/Linux). Vous pouvez le télécharger depuis la page de [téléchargement](../../../download).

Si vous souhaitez compiler Siril depuis les sources, veuillez consulter la page d'[installation](https://siril.readthedocs.io/en/stable/installation/source.html).

# Que nous réserve Siril 1.4 ?
Le développement de Siril 1.4 progresse de manière continue. Pour les plus curieux, la dernière version de développement, 1.3.6, inclut désormais un moteur de script Python. Nous encourageons tous les développeurs à commencer à écrire des scripts et à nous envoyer des retours. Votre avis est essentiel pour nous aider à améliorer cette fonctionnalité !

Restez à l'écoute pour plus de nouvelles alors que nous continuons à travailler sur la version majeure Siril 1.4. Et pour en savoir plus, vous pouvez aller consulter ce [poste](../../2024/12/les-douze-jours-de-siril/).

# Contribuer à Siril
Bien sûr, Siril est un programme informatique développé par des humains faillibles, et des bugs peuvent encore exister. Si vous pensez en avoir découvert un, veuillez nous [contacter](../../faq/#how-can-I-contact-siril-team-) ou [écrire](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) un rapport de bug s'il n'est pas déjà présent dans la [liste des bugs connus](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened). Une [page de documentation](https://siril.readthedocs.io/en/stable/Issues.html) a été rédigée pour vous aider à signaler un problème.

# Faire un don
Développer des logiciels est amusant, mais cela prend aussi la quasi-totalité de notre temps libre. Si vous aimez Siril et souhaitez nous soutenir pour poursuivre son développement, vous pouvez [faire un don](../../donate) du montant de votre choix.
