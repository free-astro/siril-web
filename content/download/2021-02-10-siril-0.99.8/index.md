---
title: Siril 0.99.8.1
author: Cyril Richard
date: 2021-02-13T08:30:00+00:00
categories:
  - News
tags:
  - new release
version: "0.99.8.1"
linux_appimage: "https://free-astro.org/download/Siril-0.99.8.1-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-0.99.8.1-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-0.99.8.1-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-0.99.8.1.tar.bz2"
---

### <span style="color:red">February 13rd, release udpate</span>

Due to a crash with some FITS files of the 0.99.8 version (generally coming from ASIAIR system), a bug fix was quickly released under the 0.99.8.1 version. We encourage everyone to update their software, even if Siril says that everything is up to date.

### The release
This is the third beta version of the upcoming 1.0 version. It comes with many improvements and bug fixes over the second beta (0.99.6) and also some over the stable version (0.9.12), and continues the big refactoring started with the 0.99 series. 

The biggest improvements are new tools for astrometry and performance and stability improvements.

We would like to emphasize the great work made by the beta testers. It allowed us to make this great release with so many bug fixes: Cécile, Colmic, Stéphiou and Trognon. Thank you!

{{< youtube id="wnY0H8w4MpM" title="Highlights of the 0.99.8 version" >}}

### Download
As a beta version of Siril 1.0, we only distribute this version with the packages for the 3 most common platforms (Windows, MacOS, GNU/Linux). See the [download page](../../../download).

But as usual, since siril is a free software, it can be built on any OS from source, see the installation page. 

## What is new in this release?

### New website
You will probably have noticed that this is a new website: the previous was heavy and hard to maintain. We decided to develope a static website. This tedious work has been done by the team of [pixls.us](https://pixls.us/) with whom we collaborate, in particular Mica and PatDavid whom we warmly thank.

### New feature of astrometry annotation and object look-up
This is probably the most important feature introduced in this new version, as it lies now at the heart of Siril. There two ways to do a plate-solving in siril: one in the "burger" menu, then *Image Information* and *Image Plate Solver...*, the other in the photometric colour calibration, only available for colour images. These tools define keywords in the FITS header that then allow Siril to create a matching between pixel position and the World Coordinate System (WCS). It can be seen while moving the pointer over an image in the right part of the status bar below it. A few new features stem from this. Saving the image in another format than FITS will make it lose this capability.
* Annotation: when plate-solving has succeeded, and also when it has been done in external software, the button {{< figure src="astrometry_button.png" >}} becomes available and clicking on it will display annotations for objects available in the image. By default, Siril ships with main catalogues, and they can be disabled in the preferences in the new *astrometry* tab.
{{< figure src="pref_astrometry_en.png" caption="Preferences are available from the 'burger' menu or with the ctrl + P shortcut.">}}
* The PSF result window has been improved with the display of the WCS coordinates of the selected object and with a link that opens a Web browser giving more information about it. {{< figure src="PSF_en.png" caption="Dialog opened when running the PSF on a star.">}} An Internet connection is required to use this, as it queries the SIMBAD database. This can be very useful for photometric analysis, to verify if a star is variable or not, and thus suitable for use as a reference or not.
* Object searching, available from the pop-up menu (right click on the image) or the ctrl + / shortcut, allows you to search an object in the image and annotate it if it's not already in Siril catalogues.

### New feature of image snapshot
To share the image at any time of the processing, with or without astrometric annotations, and with the current visualisation settings, a new snapshot feature has been added. Clicking on the button with a camera drawn on it in the header bar of the program activates it. The image is saved in the current working directory.

### Redesign of the conversion and sequence export
The conversion is a core feature of Siril; it must be able to robustly convert any image format to Siril native sequences. As a reminder, Siril can work with FITS images, SER sequences and since 0.99 versions series, FITS cube sequences, a single FITS file containing a sequence of images. Conversion has been completely remade internally and manages memory in a better way. The sequence export has also been remade to handle the many cases that are made possible with various output formats, cropping, image alignment from registration data, resizing and filtering.

### Excluding images from the sequence from the Plot
{{< figure src="plot_remove.gif">}}
It is now possible to interact with the plot, the image represented by the coordinates below the pointer gets its index displayed, and it can be excluded from the sequence and from following stacking operations by clicking on it. It can be included back from the list of images in the sequence available in the Sequence tab.

### Improvement of the colour saturation tool
{{< figure src="saturation_en.png" caption="The colour saturation tool has been slightly improved.">}}
The colour saturation tool has been improved with the addition of a new cursor defining the level of the sky background. It allows saturation to be increased on the image without increasing the saturation of the background noise.

### Improving film support
Using films as input sequence in Siril, whether they are AVI or any other file format, has been obsolete for some time, but not deactivated. The main issue with these film files is that while they provide many codecs and pixel representations, handling them in programs is complicated and there is no benefit from using them compared to the SER format that was made for astronomy uses, as explained [here](https://free-astro.org/index.php?title=SER). In this new version, using film files should actually work better than in previous ones, except parallel processing won't be available, and on opening, a dialog suggesting an automatic conversion to SER pops up.
{{< figure src="sequence_deprecated.png">}}

### New commands and commands improvements
* New [merge command](https://free-astro.org/index.php?title=Siril:Commands#merge) that merges sequences
* New [seqstat command](https://free-astro.org/index.php?title=Siril:Commands#seqstat) that computes statistics on a sequence
* Add rejection type parameters the [stack command](https://free-astro.org/index.php?title=Siril:Commands#stack)
* Fixed the [setref command](https://free-astro.org/index.php?title=Siril:Commands#setref) to work without an opened sequence

### Optimisations
* Reduced the memory used by global registration
* Improved FITS image reading time
* Reduced minimal window size
* Improve font adaptation to various screen resolutions: it is now possible to define a scaling factor for the fonts, required for high resolution displays. It is available in the preferences. {{< figure src="fonts.png" caption="Choosing font scale and other aesthetic features.">}}

### Bug fixes
* Fixed sequence analysis of PSF (seqpsf) for 32-bit images and for sequences with no registration data
* Fixed out-of-memory conditions on global registration and median or mean stacking (caused some systems to freeze)
* Fixed memory limits for 32-bit siril on 64-bit OS (available memory becomes 2GB) and fixed opening of large SER files for 32-bit siril
* Fixed minimum and maximum stacking which were broken for RGB sequences
* Fixed a crash in plate solving on second try and improve its speed
* Fixed dates in FITS cubes

The complete list of tickets (features and bugs, with details) is [here, on gitlab](https://gitlab.com/free-astro/siril/-/issues?label_name%5B%5D=beta%3A%3A0.99.8&scope=all&state=all).

### Translators
Cyril Richard, Stefan Beck, Kostas Stavropoulos, Xiaolei Wu, Joaquin Manuel, Alexandre Prokoudine, Luca Benassi, Antoine Hlmn, Kenji Iohara, Marta Fernandes

### Donate
Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development you’re welcome to [donate](../../donate) a small amount of your choice.

