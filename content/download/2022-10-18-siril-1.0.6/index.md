---
title: Siril 1.0.6
author: Cyril Richard
date: 2022-10-18T00:00:00+00:00
categories:
  - News
tags:
  - new release
aliases:
  - /download/1.0.6/
version: "1.0.6"
linux_appimage: "https://free-astro.org/download/Siril-1.0.6-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.0.6-setup.exe"
windows_portable: "https://free-astro.org/download/siril-1.0.6_win.zip"
mac_binary: "https://free-astro.org/download/siril-1.0.6-2-x86_64.dmg"
mac_arm: "https://free-astro.org/download/siril-1.0.6-2-arm64.dmg"
source_code: "https://free-astro.org/download/siril-1.0.6.tar.bz2"
---

While version 1.0.5 could be the last available version of the 1.0 branch, it turns out that some annoying bugs have been reported and needed to be treated as a priority. Siril 1.0.6 is therefore made available for bug fixing purposes only. Indeed, the development of version 1.2.0 is in full swing and currently takes us a lot of time.

We also take the opportunity of the release of this version, on October 18th, to wish a happy birthday to Cécile who has had her hands in the code of 1.2.0 for several months already, in order to make available a future version which will be incredible.

### Downloads
Siril 1.0.6 is distributed for the 3 most common platforms (Windows, MacOS, GNU / Linux). Check out the [downloads](../../../download) page.

But of course, as Siril is a free software, you can build from the sources, available through the [installation](https://free-astro.org/index.php?title=Siril:install) page.

### What’s new
- When an empty or malformed SER file was in the directory, previewing it would crash siril.
- Saving an image as TIFF could crash siril.
- Extracting the polynomial background gradient could crash siril when not enough samples were set.
- The `iif` command of Pixel Math had a problem. Also, PixelMath mishandled some negative values.
- Cropping a sequence with images of different sizes would crash if the selected area was not common to all the frames of the sequence.
- Command seqstat could crash on large sequences when some images were not selected.
- Siril could crash when a large star was close to border during star detection.
- Asinh tool had bad behaviour with the black point for monochrome and 32bits images.
- Searching for a sky object in SIMBAD containing a '+' character in its named failed.


Of course, despite all our efforts, bugs may still exist (I think this is obvious now with this unexpected release). If you think you have found one, please reach out to us through the [forum](https://discuss.pixls.us/siril). Even better, do file a bug report [there](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) if you see it is not already listed as a [known bug](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened).


**Contributors** to this release are: Cyril Richard, Cécile Melis, Vincent Hourdin, Adrian Knagg-Baugh et Fred Denjean.

### Donate 
Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development you’re welcome to [donate](../../donate) a small amount of your choice.
