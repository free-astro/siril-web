---
title: Siril 1.0.3
author: Cyril Richard
date: 2022-06-28T01:00:00+00:00
categories:
  - News
tags:
  - new release
aliases:
  - /download/1.0.3/
version: "1.0.3"
linux_appimage: "https://free-astro.org/download/Siril-1.0.3-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.0.3-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-1.0.3-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-1.0.3.tar.bz2"
---

Before the month of June comes to an end, we had to release a new version in order to keep the cruising rhythm started after the release of version 1.0.

### Downloads
Siril 1.0.3 is distributed for the 3 most common platforms (Windows, MacOS, GNU / Linux). Check out the [downloads](../../../download) page.

But of course, as Siril is a free software, you can build from the sources, available through the [installation](https://free-astro.org/index.php?title=Siril:install) page.

### So what's new
This version 1.0.3 has very few new features as it is above all a stabilization version of the 1.0.2 released last month. It contains mainly bug (and crash!!) fixes reported by users and reported on [this page](https://free-astro.org/index.php?title=Siril:1.0.2#Known_issues). However, once again, a new tool has been added: the generalized hyperbolic transformation.

Of course, despite all our efforts, bugs may still exist. If you think you have found one, please reach out to us through the [forum](https://discuss.pixls.us/c/software/siril/34). Even better, do file a bug report [there](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) if you see it is not already listed as a [known bug](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened).

#### The generalized hyperbolic transformation
[Adrian Knagg-Baugh](https://www.astrobin.com/users/ajekb78/) is the main author of the generalized hyperbolic transformation implementation in Siril. Indeed, starting from a discussion on the forum of [astrobin](https://www.astrobin.com/forum/c/astrophotography/deep-sky-processing-techniques/alternative-image-stretching-for-mathematically-bent-pixinsight-users-from-linear-to-non-linear-quicker-easier-and-better/), [David Payne](https://www.astrobin.com/users/Gunshy61/), an amateur astrophotographer, elaborated a whole series of equations using the PixelMath tool of PixInsight. Their purpose was to simplify the stretching applicable to a linear image. These equations, based on hyperbolic stretching, are perfectly suited to the stretching of astronomical linear images:
- They are piecewise continuous, which makes the result look natural.
- Their first derivative is always positive which will avoid spike and backwards stretch situation that can arise with curves.
- The equations are normalized between 0 and 1 which avoids clipping pixels and thus losing information.

Based on this work, a second contributor, [Mike Cranfield](https://www.astrobin.com/users/mike1485/), developed a script for the same software based on this suite of equations. This simplified and improved the work started.

Then Adrian coded the tool for Siril. The tool is offered in a simplified version in 1.0.3 but will exist in a much more complete implementation in the future 1.2.0, which is in very active development.

{{<figure src="dialog.png" link="dialog.png" caption="Dialog of the tool as presented in the version 1.0.3 of Siril.">}}

{{<figure src="transformation.png" link="transformation.png" caption="Example of the same image processed on one hand with the histogram transformation tool and on the other hand with the generalized hyperbolic transformation. Note that the latter preserves the size of the stars and the center of the galaxies. All the controls present in the dialog allow a fine adjustment of the image." width="100%">}}

#### A word about the future
Siril is in a phase that it has never known before: 3 developers are working hard on new features and on a complete redesign of several points. There may be as many changes, if not more, between 1.2.0 and 1.0.0 as there were between 0.9.12 and 1.0.0. The version currently under development is numbered 1.1.0. The second number, when it is odd, means that the version is said to be unstable. There are currently many changes and it would not make much sense to list them all here. However, we can list the main new features that will be present in this future version which release date is not yet known:
- Livestacking
- Refactoring of the registration part with lots of new options
- Making siril completely usable from command line, even for stacked image processing
- Astrometry and color calibration completely revised and corrected
- Libraw is no longer used for demosaicing images, only for decoding the raw file
- ...


**Contributors**:
Contributors to this release are: Cyril Richard, Cécile Melis, Vincent Hourdin, Adrian Knagg-Baugh, Mike Cranfield and David Payne. We also want to thank all the beta testers and especially Fred Denjean, for his gift to find bugs.

### Donate 
Developing software is fun, but it also takes up nearly all of our spare time. If you like Siril and would like to support us in continuing development you’re welcome to [donate](../../donate) a small amount of your choice.
