---
title: Siril 1.0.3
author: Cyril Richard
date: 2022-06-28T01:00:00+00:00
categories:
  - Nouvelles
tags:
  - new release
aliases:
  - /fr/download/1.0.3/
version: "1.0.3"
linux_appimage: "https://free-astro.org/download/Siril-1.0.3-x86_64.AppImage"
linux_binary_32: ""
linux_binary_64: ""
windows_binary: "https://free-astro.org/download/siril-1.0.3-setup.exe"
windows_xp_binary: ""
mac_binary: "https://free-astro.org/download/siril-1.0.3-x86_64.dmg"
source_code: "https://free-astro.org/download/siril-1.0.3.tar.bz2"
---

Avant que le mois de juin ne touche à sa fin, nous nous devions de sortir une nouvelle version afin de garder le rythme de croisière entamée à la suite de la sortie de la version 1.0.

### Téléchargements
Siril 1.0.3 est distribué comme d'habitude pour les 3 plates-formes les plus courantes (Windows, MacOS, GNU / Linux). Voir la page [téléchargement](../../../fr/download).

Mais bien sûr, puisque Siril est un logiciel libre, il peut être construit sur n'importe quel OS à partir des sources, voir la page [installation](https://free-astro.org/index.php?title=Siril:install/fr).

### Quelles sont les nouveautés
Sur le même principe que la version précédente, cette version 1.0.3 embarque très peu de nouveautés car c'est avant tout une version de stabilisation de la 1.0.2 sortie le mois dernier. Elle comporte donc essentiellement des correctifs de bugs (et de crash!!) recensés par les utilisateurs et reportés sur [cette page](https://free-astro.org/index.php?title=Siril:1.0.2/fr#Probl.C3.A8me_connus). Cependant, encore une fois, un nouvel outil a été ajouté : la transformation hyperbolique généralisée.

Bien sûr, Siril est un logiciel informatique développé par des humains faillibles, et des bugs peuvent encore exister. Si vous pensez en découvrir, merci de nous [contacter](../../faq/#comment-puis-je-contacter-léquipe-de-siril-) ou d'[écrire](https://gitlab.com/free-astro/siril/-/issues/new?issue%5Bmilestone_id%5D=) un rapport de bug si ce dernier n'est pas déjà présent dans la [liste des bugs connus](https://gitlab.com/free-astro/siril/-/issues?sort=updated_desc&state=opened).

#### La transformation hyperbolique généralisée
[Adrian Knagg-Baugh](https://www.astrobin.com/users/ajekb78/) est l'auteur principal de l'implémentation de la transformation hyperbolique généralisée dans Siril. En effet, partant d'une discussion sur le forum du site [astrobin](https://www.astrobin.com/forum/c/astrophotography/deep-sky-processing-techniques/alternative-image-stretching-for-mathematically-bent-pixinsight-users-from-linear-to-non-linear-quicker-easier-and-better/), [David Payne](https://www.astrobin.com/users/Gunshy61/), un astrophotographe amateur, a élaboré toute une série d'équations à l'aide de l'outil PixelMath de PixInsight. Elles avaient pour but de simplifier l'étirement applicable à une image linéaire. Ces équations, basées sur un étirement hyperbolique, conviennent parfaitement à l'étirement des images astronomiques:
- Elles sont continues par morceau, ce qui donne un aspect naturel au résultat.
- Leur dérivée première est toujours positive ce qui empêche la création d'artefact de "rebond".
- Les équations sont normalisées entre 0 et 1 ce qui évite de clipper des pixels et ainsi de perdre de l'information.

Fort de ce travail, un second contributeur, [Mike Cranfield](https://www.astrobin.com/users/mike1485/), a développé un script pour PixInsight en se basant sur cette suite d'équations, simplifiant et améliorant ainsi le travail amorcé.

C'est alors qu'Adrian a codé l'outil pour Siril. Ce dernier est proposé en version simplifiée dans cette version 1.0.3 afin de ne pas ajouter de bugs à cette version stable, mais existera en version bien plus complète dans la future 1.2.0, qui est en développement très actif.

{{<figure src="dialog.fr.png" link="dialog.fr.png" caption="Fenêtre de l'outil tel que présenté dans la version 1.0.3 de Siril.">}}

{{<figure src="transformation.png" link="transformation.png" caption="Exemple d'une même image traitée d'une part avec l'outil de transformation de l'histogramme et d'autre part avec la transformation hyperbolique généralisée. On note que cette dernière préserve la taille des étoiles ainsi que le centre des galaxies. Tous les contrôles présents dans le dialogue permettent un ajustement fin de l'image." width="100%">}}

#### Un mot sur le futur
Siril est dans une phase qu'il n'a actuellement jamais connu : 3 développeurs travaillent d'arrache-pied sur des nouvelles fonctionnalités et sur une refonte complète de plusieurs points. Il y aura peut être autant de changements, si ce n'est plus, entre la version 1.2.0 et la version 1.0.0 qu'il y'en avait etre la 0.9.12 et la 1.0.0. La version actuellement en développement porte le numéro de version 1.1.0. Le deuxième chiffre, lorsqu'il est impair, signifie que la version est dite instable. Elle comporte actuellement de nombreux changements et tous les citer ici n'aurait pas beaucoup de sens. Cependant, nous pouvons lister les principales nouveautés qui seront présentes dans cette future version dont la date de sortie n'est encore pas connue :
- Livestacking
- Refactoring de la partie alignement avec plein de nouvelles options
- Rendre siril complètement utilisable en ligne de commande, même pour le traitement d'images empilées
- Une astrométrie et un étalonnage des couleurs entièrement revus et corrigés
- Libraw n'est plus utilisée pour le dématriçage des images, seulement pour le décodage du fichier raw
- ...


**Contributeurs**:
Les contributeurs de cette version sont : Cyril Richard, Cécile Melis, Vincent Hourdin, Adrian Knagg-Baugh, Mike Cranfield et David Payne. Nous tenons également à remercier tous les bêta-testeurs et notamment Fred Denjean, pour son don à trouver les bugs.

### Dons
Développer des logiciels est amusant, mais cela nous prend aussi presque tout notre temps libre. Si vous aimez Siril et que vous souhaitez nous soutenir dans la poursuite de son développement, vous pouvez faire un [don](../../donate) d’une somme de votre choix.
